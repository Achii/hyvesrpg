import Vue from 'vue';
import Router from 'vue-router';
import Login from '../components/Login'
import Register from '../components/Register'
import Dashboard from '../components/Dashboard'
import Feeds from '../components/Feeds'

import Storyline from '../components/Storyline'

import firebase from 'firebase/app'
import 'firebase/auth'

Vue.use(Router)

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
    {
        path:"*",
        redirect: "/login"
    },
    {
        path: '/login',
        name: 'login',
        component: Login
    },
    {
        path: '/register',
        name: 'Register',
        component: Register
    },
    {
        path: '/dashboard',
        name: 'Dashboard',
        component: Dashboard,
        meta: {
            requiresAuth: true
          },
        children: [
        ],
    },
    {
        path: '/feeds',
        name: 'Feeds',
        component: Feeds,
        meta: {
            requiresAuth: true
          },
    },
    {
        path: '/storyline',
        name: 'Storyline',
        component: Storyline,
        meta: {
            requiresAuth: true
          },
    }
]
});


router.beforeEach(async (to, from, next) => {
    const currentUser = firebase.auth().currentUser;
    const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
    if(requiresAuth && !currentUser) next('login');
    else if (!requiresAuth && currentUser) next('dashboard');
    else next();
  })

export default router