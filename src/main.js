import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'

import "@fortawesome/fontawesome-free/css/all.css";

import Vue from 'vue'
import App from './App.vue'
import router from "./routes/index";
import * as firebase from "firebase/app";
import store from "./store";

Vue.config.productionTip = false


const firebaseConfig = {
  apiKey: "AIzaSyCFQr1oDVjv4k_mAL70NPx5XCZgAQsu3zI",
  authDomain: "extended-web-256820.firebaseapp.com",
  databaseURL: "https://extended-web-256820.firebaseio.com",
  projectId: "extended-web-256820",
  storageBucket: "extended-web-256820.appspot.com",
  messagingSenderId: "711003286016",
  appId: "1:711003286016:web:a0a2c89711f6e91cb0c6e5"
};

let app = '';
firebase.initializeApp(firebaseConfig);

firebase.auth().onAuthStateChanged(user => {

  if(user){
    firebase.firestore()
    .collection('users')
    .doc(user.uid)
    .get()
    .then(snapshot => {
      const document = snapshot.data()
      let profile = document.profile
      store.dispatch("fetchProfile", profile);
      
    })
  }


    store.dispatch("fetchUser", user);
  

  if (!app) {
    app = new Vue({
      router,
      store,
      render: h => h(App),
    }).$mount('#app')
  }
});


